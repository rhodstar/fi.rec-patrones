function [finalCentroids,finalRegions] = lgb(X,num_regions,epsilon,threshold)
  iterator = log2(num_regions);
  
  c1 = mean_centroid(X);
  centroids = [c1];
  for k=1:iterator
     [centroids, regions] = get_stable_regions(X,centroids,epsilon,threshold);
  end
  finalCentroids = centroids;
  finalRegions = regions;
end

function [centroids, regions] = get_stable_regions(X,centroids,epsilon,threshold)
  dg_acum = 0;

  centroids = offset_centroids(centroids,epsilon);
  regions = cell(length(centroids),1);
  
  for k=1:threshold
    for j=1:length(X.x)
      point = get_point(X,j);
      [centroidIndex, dMin] = findCorrespondingCentroid(centroids,point);
      regions{centroidIndex}(end + 1) = point;
      dg_acum = dg_acum + dMin;
    end  
    centroids = calculateCentroidRegions(regions);
  end
  
end

function centroid = mean_centroid(X)
  centroid.x = mean([X.x]);
  centroid.y = mean([X.y]);
end

function offsetCentroids = offset_centroids(centroids, epsilon)
  tmpCentroids = struct('x', zeros(1, 0), 'y', zeros(1,0));
  index = 1;
  for j=1:length(centroids)
    tmpCentroids(index).x = centroids(j).x + epsilon{1}.x;
    tmpCentroids(index).y = centroids(j).y + epsilon{1}.y;
    index = index + 1;
    tmpCentroids(index).x = centroids(j).x + epsilon{2}.x;
    tmpCentroids(index).y = centroids(j).y + epsilon{2}.y;
    index = index + 1;
  end
  offsetCentroids = tmpCentroids;
end

function point = get_point(X, index)
    point.x = X.x(index);
    point.y = X.y(index);
end

function [k,dgMin] = findCorrespondingCentroid(centroids,element)
  centroidIndex = 1;
  dMin = euclidean_distance(centroids(1),element);
  for j=2:length(centroids)
    d = euclidean_distance(centroids(j),element);
    if dMin > d
      dMin = d;
      centroidIndex = j;
    end
  end
  k = centroidIndex;
  dgMin = dMin;
end

function d = euclidean_distance(p1,p2)
  xDiff = (p2.x - p1.x)^2;
  yDiff = (p2.y - p1.y)^2;
  d = sqrt(xDiff + yDiff);
end

function newCentroids = calculateCentroidRegions(G_regions)
  tmpCentroids = struct('x', zeros(1, 0), 'y', zeros(1,0));
  for j=1:length(G_regions)
    tmp = mean_centroid(G_regions{j});
    tmpCentroids(j).x = tmp.x;
    tmpCentroids(j).y = tmp.y;
  end
  newCentroids = tmpCentroids;
end