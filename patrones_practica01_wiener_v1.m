% Autor:        Rodrigo Francisco <rhodfra@gmail.com>
% Fecha:        03/04/2021
% Descripcion:  Programa para obtener los pesos w 
%               a traves del filtro de Wiener

clear all; clc
format short g

%%&&& Parametros
num_puntos = 2000;
N = 300;                % Numero de muestras


%%%%% Generacion de ruido blanco v(n)

v = generador_ruido(num_puntos);


%%%%% Generando la senal x(n) con el ruido blanco
% x(1) = x(2) = 0;
x =[0, 0];

for n=3:1:num_puntos
  x(n) = 0.6530*x(n-1)-0.7001*x(n-2) + v(n);
end

figure('Name','x(n)');
plot(1:length(x),x);
title('x(n)')

%%%%% Construyendo la matriz de Toeplitz

R = [
  r_t(0,N,x) r_t(1,N,x);
  r_t(-1,N,x) r_t(0,N,x)
];


%%%%% Generando r vector (vector de correlaciones)

r = [ r_t(-1,N,x) ; r_t(-2,N,x)];


%%%%% Encontrando los pesos w

w = inv(R)*r;


%%%%% Generando la senal y(n) con los pesos w


for n=3:1:num_puntos
  y(n) = w(1)*x(n-1)-w(2)*x(n-2);
end

%%%%% Mostrando algunos resultados

R

r

w


figure('Name','x(n) vs y(n)')
subplot(1,2,1)
plot(1:length(x),x)
title('x(n)')
subplot(1,2,2)
plot(1:length(y),y)
title('y(n)')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%                  Funciones utiles                     %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Funcion para generar ruido blanco

function ruido = generador_ruido(num_puntos)
  for n=1:num_puntos
    v(n) = randn;
  end
  ruido = v;
end


%%%%% Funcion para calcular la correracion
% apartir de N y del valor de tau (representada como t)

function corr = r_t(t,N,x) 
  tmp = 0;
  for i=3:N-1-3
    tmp = tmp + x(i)*x(i+t);
  end
  corr = tmp/N;
end    
