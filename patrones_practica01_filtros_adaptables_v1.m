% Autor:        Rodrigo Francisco <rhodfra@gmail.com>
% Fecha:        03/04/2021
% Descripcion:  Programa para obtener los pesos w 
%               a través de filtros adaptables

clear all; clc
format short g

%%&&& Parametros
num_puntos = 2000;

%%%%% Generación de ruido blanco

v = generador_ruido(num_puntos);


%%%%% Generando la señal x(n) con el ruido blanco
% x(1) = x(2) = 0;
x = [0,0];

for n=3:1:num_puntos
  x(n) = 0.6530*x(n-1)-0.7001*x(n-2) + v(n);
end

% w0(1) = w0(2) = w0(3) = 0;
w0 = [0,0,0];

% w1(1) = w2(2) = w3(3) = 0;
w1 = [0,0,0];

% e(1) = e(2) = 0;
e = [0,0];

% xHat(1) = xHat(2) = 0;
xHat = [0,0];

%%%%% Parametros de LMS iterativo
mu = 0.01;
N = 1000;

for n=3:N
  xHat(n) = w0(n)*x(n-1) + w1(n)*x(n-2);
  e(n) = x(n) - xHat(n);
  w0(n+1) = w0(n) + mu*x(n-1)*e(n);
  w1(n+1) = w1(n) + mu*x(n-2)*e(n);
end

% Los pesos finales w son:
wo_N = w0(length(w0))
w1_N = w1(length(w1))

figure('Name','w0, w1 & e(n)')
subplot(2,2,1)
plot(1:length(w0),w0)
annotation('textbox', [.2, .44, .1, .1], 'String', "mu  = "+mu)
title('w0')
subplot(2,2,2)
plot(1:length(w1),w1)
title('w1')
subplot(2,2,3:4)
plot(1:length(e),abs(e))
title('e(n)')

% figure('Name','x(n) vs xHat(n) = y(n)')
% subplot(1,2,1)
% plot(1:length(x),x)
% title('x(n)')
% subplot(1,2,2)
% plot(1:length(xHat),xHat)
% title('xHat(n)')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%                  Funciones utiles                     %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Funcion para generar ruido blanco

function ruido = generador_ruido(num_puntos)
  for n=1:num_puntos
    v(n) = randn;
  end
  ruido = v;
end
